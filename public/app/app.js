'use strict';

var myApp = angular.module('myApp', ['ngRoute', 'echonest', 'spotify']);

//configuration/router
myApp.config(function($routeProvider, $locationProvider) {
  
  $routeProvider
    .when('/', {
      controller: 'MainController',
      templateUrl: 'app/views/main.html'
    })
    .when('/artist', {
      controller: 'ArtistController',
      templateUrl: 'app/views/sections/artist.html'
    })
    .when('/about', {
      controller: 'MainController',
      templateUrl: 'app/views/sections/about.html'
    })
    .otherwise({
      redirectTo: '/'
    });
});


myApp.factory('SharedDataService', function() {
  
  var artistData = {};
  
  return {
    getArtistData: function() {
      return artistData;         
    },
    
    setArtistSpotifyData: function(name, id) {
      artistData.name = name;
      artistData.spotifyId = id;
    },
    
    setArtistEchonestData: function(id) {
      artistData.echonestId = id;
    }
  };
});



/** controller **/
myApp.controller('MainController', ['$scope', '$location', 'EchonestService', 'SpotifyService', 'SharedDataService', 
                                    function($scope, $location, EchonestService, SpotifyService, SharedDataService) {
  
  $scope.showResults = false;
  $scope.artist = {};
  
  $scope.artists = [];
  $scope.albums = [];
  $scope.tracks = [];
  
  $scope.searchField = '';
  
  
  $scope.value = '';
  $scope.artistIdEchoNest = '';
  

    
  $scope.fetchArtist = function(name, id) {

    //Find id from Echonest for artist, then set name, spotify id, and echonest id to object
    EchonestService.artistSearch(name).then(function(results) {
      
      SharedDataService.setArtistSpotifyData(name, id);
      SharedDataService.setArtistEchonestData(results.data.response.artists[0].id);
      
      $location.path('/artist');
    });
  };  
  
  
  $scope.search = function() {
    
    if(!$scope.searchField) {
      alert('need a search field');
      return;
    }
    
    SpotifyService.search($scope.searchField).then(function(results) {
      
      $scope.artists = results.data.artists;
      $scope.albums = results.data.albums;
      $scope.tracks = results.data.tracks;
      $scope.showResults = true;
    });
  };
}]);


myApp.controller('ArtistController', ['$scope', '$location', '$route', 'SpotifyService', 'EchonestService', 'SharedDataService', 
      function($scope, $location, $route, SpotifyService, EchonestService, SharedDataService) {
  
  //artist object contains spotifyid, echonestid, and name
  $scope.artist = SharedDataService.getArtistData();
  
        
  $scope.loadArtist = function(name, id) {
    
    $scope.artist = {};
  
    //Find id from Echonest for artist, then set name, spotify id, and echonest id to object
    EchonestService.artistSearch(name).then(function(results) {
      
      SharedDataService.setArtistSpotifyData(name, id);
      SharedDataService.setArtistEchonestData(results.data.response.artists[0].id);
      
      $route.reload();
    });
  };      
        
  
  $scope.getAlbumTracks = function(id) {
    
    SpotifyService.getAlbumTracks(id).then(function(results) {
      
      $scope.albumTracks = results.data.tracks;
      console.log(results.data.tracks);
    });
  };
  
  //the GET /artists endpoint doesn't send much data; just pluck what is neeed
  SpotifyService.getArtist($scope.artist.spotifyId).then(function(results) {

    $scope.artist.genres = results.data.genres;
    $scope.artist.images = results.data.images;
    
  }).then(function() {  
    
      //get an artist's biography - uses echonest
      EchonestService.getArtistBiographies($scope.artist.echonestId).then(function(results) {

        $scope.artist.biographies = [];
        var bios = results.data.response.biographies;
        var total = bios.length;
        
        //filter out bios that are too short
        for(var i = 0; i < total; i++) {
          
          if(bios[i].text.length < 100) {
            continue;  
          } else {
            $scope.artist.biographies.push(bios[i]);
          }
        }
      });
    
      //get an artist's reviews - uses echonest
      EchonestService.getArtistReviews($scope.artist.echonestId).then(function(results) {
        
        $scope.artist.reviews = [];
        var reviews = results.data.response.reviews;
        var total = reviews.length; 
        
        //filter out reviews that are too short
        for(var i = 0; i < total; i++) {
          
          if(reviews[i].summary.length < 100) {
            continue;
          } else {
            $scope.artist.reviews.push(reviews[i]);
          }
        }
      });
      
      //get an artists's urls - uses echonest
      EchonestService.getArtistUrls($scope.artist.echonestId).then(function(results) {
        $scope.artist.urls = results.data.response.urls;
      });
      
      //get an artist's images - uses echonest
      EchonestService.getArtistImages($scope.artist.echonestId).then(function(results) {
        $scope.artist.extraImages = results.data.response.images;
      });
      
      //get an artist's videos - uses echonest
      EchonestService.getArtistVideos($scope.artist.echonestId).then(function(results) {
        $scope.artist.videos = results.data.response.video;
      });
      
      //get artists that are similar - uses echonest
      EchonestService.getArtistSimilar($scope.artist.echonestId).then(function(results) {
        $scope.artist.extraSimilar = results.data.response.artists;
      });
      
      //get albums for an artist
      SpotifyService.getAlbums($scope.artist.spotifyId).then(function(results) {
        $scope.artist.albums = results.data;
      });

      //get related artists
      SpotifyService.getRelated($scope.artist.spotifyId).then(function(results) {
        $scope.artist.related = results.data.artists;
      });
    
    }).then(function() {
        console.log($scope.artist);
    });
    
}]);


/** directives **/
myApp.directive('artistSearchResults', function() {
  return {
    restrict: 'E',
    templateUrl: 'app/views/artist-search.html'
  };
});

myApp.directive('albumSearchResults', function() {
  return {
    restrict: 'E',
    templateUrl: 'app/views/album-search.html'
  };
});

myApp.directive('trackSearchResults', function() {
  return {
    restrict: 'E',
    templateUrl: 'app/views/track-search.html'
  };
});



myApp.filter('convertToTime', function() {
  return function(millis) {
    var min = parseInt(millis / 1000 / 60);
    var sec = parseInt(millis / 1000 % 60);
    return min + ':' + ('00' + sec).slice(-2);
  };
});


myApp.filter('urlTitle', function() {
  return function(url) {
    return url.split('_')[0].toUpperCase();
  };
});
