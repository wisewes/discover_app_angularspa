var app = angular.module('spotify', []);

app.factory('SpotifyService', ['$http', function($http) {
  
  var Config = {
    clientID:     'fc83fd037cbd4e14bf981b1b81268df4',
    clientSecret: 'ee858869efc84213a2efabc013834918',
    redirectURL:  'http://mysite.com/callback',
    baseEndpoint: 'https://api.spotify.com/v1/'
  };
  
  var Endpoints = {
    search: Config.baseEndpoint + 'search',
    artists: Config.baseEndpoint + 'artists/', // + :id
    albums: Config.baseEndpoint + 'albums/' // + :id
  };
  
  
  var factory = {};
  
  factory.search = function(searchTerm) {
    
    return $http.get(Endpoints.search, { params: { q: searchTerm, type: 'artist,album,track' }});
  };
  
  factory.getArtist = function(artistId) {
  
    return $http.get(Endpoints.artists + artistId);
  };
  
  factory.getAlbums = function(artistId) {
    
    return $http.get(Endpoints.artists + artistId + '/albums', { params: { limit: 50, market: 'US'   }});
  };
  
  factory.getAlbumTracks = function(albumId) {
    
    return $http.get(Endpoints.albums + albumId, { params: { limit: 50 }});
  };
  
  factory.getRelated = function(artistId) {
  
    return $http.get(Endpoints.artists + artistId + '/related-artists');
  };
  
  return factory;
}]);