/**

Echohost Module - Factory for consuming REST API

**/

var app = angular.module('echonest', []);

app.factory('EchonestService', ['$http', function($http) {
  
  var Config = {
      apiKey:       'ASGIGF8WY5HB0EMNG',
      consumerKey:  '5aaccde8a0e61ba8c7b8f3139b6d653b',
      baseEndpoint: 'http://developer.echonest.com/api/v4/'
  };
  
  var Endpoints = {
    
    artist: {
      search:       Config.baseEndpoint + 'artist/search',
      biographies:  Config.baseEndpoint + 'artist/biographies',
      images:       Config.baseEndpoint + 'artist/images',
      profile:      Config.baseEndpoint + 'artist/profile',
      similar:      Config.baseEndpoint + 'artist/similar',
      urls:         Config.baseEndpoint + 'artist/urls',
      images:       Config.baseEndpoint + 'artist/images',
      videos:       Config.baseEndpoint + 'artist/video',
      reviews:      Config.baseEndpoint + 'artist/reviews'
    },
    
    genre: {
      search:   Config.baseEndpoint + 'genre/search',
      list:     Config.baseEndpoint + 'genre/list',
      profile:  Config.baseEndpoint + 'genre/profile'
    },
    
    song: {
      search:   Config.baseEndpoint + 'song/search',
      profile:  Config.baseEndpoint + 'song/profile'
    }
  };
  

  var factory = {};
  
  //API - search
  factory.artistSearch = function(artistName) {
    
    return $http.get(Endpoints.artist.search, { params: { api_key: Config.apiKey, name: artistName }});
  };
  
  factory.getArtistBiographies = function(id) {
  
    return $http.get(Endpoints.artist.biographies, { params: { api_key: Config.apiKey, id: id }}); 
  };
  
  factory.getArtistImages = function(id) {
  
    return $http.get(Endpoints.artist.images, { params: { api_key: Cconfig.apiKey, id: id }});
  };
                     
  factory.getArtistProfile = function(id) {
    
    return $http.get(Endpoints.artist.profile, { params: { api_key: Config.apiKey, id: id }});
  };

  factory.getArtistSimilar = function(id) {
    
    return $http.get(Endpoints.artist.similar, { params: { api_key: Config.apiKey, id: id }});
  };
  
  factory.getArtistUrls = function(id) {
    
    return $http.get(Endpoints.artist.urls, { params: { api_key: Config.apiKey, id: id }});
  };
  
  factory.getArtistVideos = function(id) {
    
    return $http.get(Endpoints.artist.videos, { params: { api_key: Config.apiKey, id: id }});
  };
  
  factory.getArtistImages = function(id) {
    
    return $http.get(Endpoints.artist.images, { params: { api_key: Config.apiKey, id: id }});
  };
  
  factory.getArtistReviews = function(id) {
  
    return $http.get(Endpoints.artist.reviews, { params: { api_key: Config.apiKey, id: id }});
  };


  //API - genre
  
  //API - song
  
  return factory;
}]);